FROM node:11-alpine as builder

RUN mkdir -p /build
COPY . /build
WORKDIR /build
RUN yarn install --pure-lockfile --production && yarn build

FROM node:11-alpine

RUN mkdir -p /app && chown node:node /app

WORKDIR /app
COPY --from=builder /build/public public
COPY prod server.js ./
RUN yarn install --pure-lockfile --production

USER node

CMD [ "sh", "-c", "yarn start" ]
