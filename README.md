# Minnie
React-based WordPress Client

Demo: https://aziz.im

## Installation

### Server
1. Copy and customize the example [Nginx config](etc/nginx.conf).
2. Restart Nginx service.

### WordPress
1. Install and activate [Minnie theme](https://github.com/kucrut/wp-minnie).
2. Go to *Appearance* > *Menus*, create two menus and assign them to the *Social* and *Primary* locations.
3. Install and activate these plugins:
  - [WP REST API](https://wordpress.org/plugins/rest-api/)
  - [Bridge](https://github.com/kucrut/wp-bridge)
  - [Bridge: Menus](https://github.com/kucrut/wp-bridge-menus)
  - [Bridge: Post Formats](https://github.com/kucrut/wp-bridge-post-formats)
4. Go to *Settings* > *Permalinks* and set the structure to `/blog/%postname%`

### Minnie
1. Download the release
2. Copy `config.sample.json` to `config.json`. Use your WordPress site's URL as the value of `siteUrl`.
3. Run `yarn install`
4. Run `yarn start`, or
	* Customize `process.json`.
	* Install [pm2](http://pm2.keymetrics.io/): `yarn global add pm2`
	* Run `pm2 start process.json`

## Developing
1. Run `yarn install && yarn dev`.
2. Develop away.

## Contributing
...

## License
[GPL v3](http://www.gnu.org/licenses/gpl-3.0.en.html)
